import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import java.util.concurrent.TimeUnit;

public class DriverManager {


    public static EventFiringWebDriver getConfigureDriver(WebDriver browser) {
        EventFiringWebDriver driver = new EventFiringWebDriver(browser);
        driver.register(new EventHandler());

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        return driver;
    }
}
                                