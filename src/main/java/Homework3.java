import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;


public class Homework3 {
    public static final String URL = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";
    public static final String LOGIN = "webinar.test@gmail.com";
    public static final String PASSWORD = "Xcg7299bnSmMuRLp9ITw";
    public static Scanner scanner = new Scanner(System.in);
    public static WebDriver driver = DriverManager.getConfigureDriver(browser());
    public static WebDriverWait wait = new WebDriverWait(driver, 10);


    public static void main(String[] args) {
        Random random = new Random();
        String categoryName = "MyNewCat" + random.nextInt(10)
                + random.nextInt(10) + random.nextInt(10);
        System.out.println("categoryName = " + categoryName);

        driver.get(URL);
        logIn();

        moveToCategoryPage();
        createNewCategory(categoryName);

        moveToCategoryPage();
        searchInTheCategoryTable(categoryName);

        logOut();
        driver.quit();
    }


    public static WebDriver browser() {
        System.out.println("Choose the browser for test execution");
        System.out.println("1 - Google Chrome");
        System.out.println("2 - Mozilla Firefox");
        System.out.println("3 - Internet Explorer");
        System.out.println("Enter number");
        boolean continueLoop = true;
        WebDriver s = null;
        do {
            try {
                byte b = scanner.nextByte();
                switch (b) {
                    case 1:
                        System.out.println("The Chrome browser is selected");
                        continueLoop = false;
                        s = new ChromeDriver();
                        break;
                    case 2:
                        System.out.println("The Firefox browser is selected");
                        continueLoop = false;
                        s = new FirefoxDriver();
                        break;
                    case 3:
                        System.out.println("The Internet Explorer browser is selected");
                        continueLoop = false;
                        s = new InternetExplorerDriver();
                        break;
                    default:
                        System.out.println("Enter one of the numbers: 1, 2 or 3");
                }
            } catch (InputMismatchException e) {
                System.out.println("Exception : " + e);
                scanner.nextLine();
                System.out.println("Enter one of the numbers: 1, 2 or 3");
            }
        } while (continueLoop);
        return s;
    }

    public static void logIn() {
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys(LOGIN);
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys(PASSWORD);
        WebElement submitButton = driver.findElement(By.name("submitLogin"));
        submitButton.click();

    }

    public static void logOut() {
        waitingById("header_employee_box");
        driver.findElement(By.id("header_employee_box")).click();
        driver.findElement(By.id("header_logout")).click();
    }

    public static void moveToCategoryPage() {
//        Move to the catalog in the main menu
        waitingById("subtab-AdminCatalog");
        WebElement menuitem = driver.findElement(By.id("subtab-AdminCatalog"));
        Actions builder = new Actions(driver);
        builder.moveToElement(menuitem).build().perform();
//        Waiting for the catalog-link appearing
        waitingById("subtab-AdminCategories");
        driver.findElement(By.id("subtab-AdminCategories")).click();
    }

    private static void createNewCategory(String categoryName) {
        waitingById("page-header-desc-category-new_category");

//        Move to the new category page
        driver.findElement(By.id("page-header-desc-category-new_category")).click();
        waitingById("name_1");
        WebElement catNameFound = driver.findElement(By.id("name_1"));
        catNameFound.sendKeys(categoryName);
        catNameFound.submit();
        //       waitingByXpath("//*[@id=\"content\"]/div[3]/div");
        //       WebElement alertMessage = driver.findElement(By.xpath("//*[@id=\"content\"]/div[3]/div"));
        waitingByClassName("alert");
        WebElement alertMessage = driver.findElement(By.className("alert"));
        System.out.println("The message '" + alertMessage.getText() + "' is displayed");
    }

    private static void searchInTheCategoryTable(String categoryName) {
        waitingByName("categoryFilter_name");
        WebElement inputCategoryName = driver.findElement(By.name("categoryFilter_name"));
        inputCategoryName.sendKeys(categoryName);
        inputCategoryName.submit();

        try {
//            Waiting until the search page is loaded
            wait.until(ExpectedConditions.urlContains("#category"));
            waitingById("table-category");
            String catName = driver.findElement(By.xpath("//tbody//td[3]")).getText();
            System.out.println("catName = " + catName);
            if (catName.equals(categoryName)) {
                System.out.println("The '" + categoryName + "' category has been succesfully created");
            } else System.out.println("The '" + categoryName + "' category has not been ceated");
        } catch (NoSuchElementException e) {
            System.out.println("Exception: " + e);
        }
    }

    public static void waitingByClassName(String className) {
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.className(className)));
    }

    public static void waitingById(String id) {
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }

    public static void waitingByName(String name) {
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
    }


}
